

const IMPLICIT = '<implicit>';

const formatField = (field) => field === IMPLICIT ? '' : (field + ':');
const formatOperator = (operator) => operator === IMPLICIT ? '' : operator;

const formatTerm = (ast) => {
  let result = '';
  if (ast.prefix) {
    result += ast.prefix;
    if (ast.prefix === 'NOT') {
      result += ' ';
    }
  }
  if (ast.quoted) {
    result +=`"${ast.term}"`;
  } else {
    result += ast.term;
  }
  if (ast.similarity) {
    result += '~';
    if (ast.similarity !== 0.5) {
      result += ast.similarity;
    }
  }
  if (ast.proximity) {
    result += '~';
    result += ast.proximity;
  }
  if (ast.boost) {
    result += '^';
    result += ast.boost;
  }
  return result;
};

const computeRangeOperator = (ast) => {
  if (ast.term_min === '*') {
    return [ast.inclusive === 'both' ? '<=' : '<', ast.term_max];
  } else if (ast.term_max === '*') {
    return [ast.inclusive === 'both' ? '>=' : '>', ast.term_min];
  }
  return ['between', null];
};
const formatRange = (ast) => {
  const [operator,val] = computeRangeOperator(ast);
  if (operator === 'between') {
    const left = ast.inclusive === 'left' || ast.inclusive === 'both' ? '[' : '{';
    const right = ast.inclusive === 'right' || ast.inclusive === 'both' ? ']' : '}';
    return `${left}${ast.term_min} TO ${ast.term_max}${right}`;
  }
  return `${operator}${val}`;
};

const formatRegex = (ast) => {
  return `/${ast.term}/`;
};

const formatGroup = (ast) => {
  if (ast.term_min) {
    return formatRange(ast);
  } if (ast.regex) {
    return formatRegex(ast);
  } else {
    return formatTerm(ast);
  }
};

const formatNode = (ast) => {
  let result = '';

  if (ast.field) {
    result += formatField(ast.field);
  }

  if (ast.parenthesized) {
    result += '(';
  }

  if (!ast.left) {
    result += formatGroup(ast);
  } else {
    result += [formatNode(ast.left), formatOperator(ast.operator), formatNode(ast.right)].filter(term => term).join(' ');
  }

  if (ast.parenthesized) {
    result += ')';
  }
  return result;
};

module.exports = function toString(ast) {
  if (!ast) {
    return '';
  }
  return formatNode(ast);
};
