

const IMPLICIT = '<implicit>';
const DEFAULT_FIELD = '<implicit>';
let currentField = DEFAULT_FIELD;

const isInverted = (prefix) => {
  const invertedPrefix = new Set(['-', 'NOT', '!']);

  return invertedPrefix.has(prefix);
};

const invertedOperators = {
  '=': '!=',
  '>': '<=',
  '>=': '<',
  '<': '>=',
  '<=': '>',
  'between': 'notBetween'
};

const computeField = (ast) => ast.field === IMPLICIT ? currentField : ast.field;
const formatTerm = (term) => term === '""' ? '' : term;

const formatField = ({prefix, similarity, proximity, ...ast}, result) => {
  const field = computeField(ast);

  if (similarity || proximity) {
    result.rules.push({
      field,
      operator: '~',
      value: ast.term,
      ...similarity && ({similarity}),
      ...proximity && ({proximity})
    });
  } else {
    const operator = isInverted(prefix) ? invertedOperators['='] : '=';

    result.rules.push({
      field,
      operator,
      value: formatTerm(ast.term)
    });
  }
};

const computeRangeOperator = ({prefix, ...ast}) => {
  if (isInverted(prefix)) {
    const operator = computeRangeOperator(ast);
    return invertedOperators[operator];
  }
  if (ast.term_min === '*') {
    return ast.inclusive === 'both' ? '<=' : '<';
  } else if (ast.term_max === '*') {
    return ast.inclusive === 'both' ? '>=' : '>';
  }
  return 'between';
};

const computeRangeValue = (ast) => {
  return [ast.term_min, ast.term_max].filter(term => term !== '*').map(formatTerm).join(',');
};
const formatRange = (ast, result) => {
  const field = computeField(ast);
  const operator = computeRangeOperator(ast);
  const value = computeRangeValue(ast);

  result.rules.push({
    field,
    operator,
    value
  });
};

const formatGroup = (ast, result) => {
  if (ast.term_min || ast.term_max) {
    formatRange(ast, result);
  } else {
    formatField(ast, result);
  }
};

const operators = {
  '&&': 'and',
  '||': 'or',
  'AND': 'and',
  'OR': 'or',
  '<implicit>': '<implicit>',
};
const computeOperator = (operator) => {
  return operators[operator];
};

const formatNode = (ast, result) => {
  let subResult = result;
  const previousField = currentField;
  if (ast.parenthesized) {
    subResult = {rules: []};
    if (ast.field && ast.field !== IMPLICIT) {
      currentField = ast.field;
    }
  }

  if (!ast.left) {
    formatGroup(ast, subResult);
  } else {
    formatNode(ast.left, subResult);
    subResult.rules.push(computeOperator(ast.operator));
    formatNode(ast.right, subResult);
  }

  if (ast.parenthesized) {
    result.rules.push(subResult);
    if (ast.field !== IMPLICIT) {
      currentField = previousField;
    }
  }
};

module.exports = function toQueryRules(ast) {
  const result = {rules: []};
  if (ast && Object.keys(ast).length) {
    formatNode(ast, result);
  }
  return result;
};
