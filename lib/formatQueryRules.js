const IMPLICIT = '<implicit>';

const formatField = (field) => !field || field === IMPLICIT ? '' : (field + ':');
const formatOperator = (operator) => operator === IMPLICIT ? '' : operator.toUpperCase();
const formatValue = (value) => !value || value === '' ? '""' : value;


const quotedChars = [' ', '<', '>', '='];

const formatEqualsRule = (rule) => {
  if (rule.value === null || rule.value === undefined || quotedChars.some(char => rule.value.includes(char))) {
    return `${formatField(rule.field)}"${formatValue(rule.value)}"`;
  }
  return `${formatField(rule.field)}${formatValue(rule.value)}`;
};

const formatBetweenRule = (rule) => {
  const values = rule.value.split(',');
  return `${formatField(rule.field)}[${formatValue(values[0])} TO ${formatValue(values[1])}]`;
};

const formatFuzzy = (rule) => {
  if (rule.similarity) {
    return '~' + (rule.similarity === 0.5 ? '' : rule.similarity);
  }
  if (rule.proximity) {
    return '~' + rule.proximity;
  }
};

const formatFieldRule = (rule) => {
  if (rule.operator === '=') {
    return formatEqualsRule(rule);
  }
  if (rule.operator === '!=') {
    return '-' + formatEqualsRule(rule);
  }
  if (rule.operator === 'between') {
    return formatBetweenRule(rule);
  }
  if (rule.operator === 'notBetween') {
    return '-' + formatBetweenRule(rule);
  }
  if (rule.operator === '~') {
    return formatEqualsRule(rule) + formatFuzzy(rule);
  }
  return `${formatField(rule.field)}${formatOperator(rule.operator)}${formatValue(rule.value)}`;
};

module.exports = function formatQueryRules(rules) {
  if (!rules) {
    return '';
  }
  const ruleStrings = rules.rules.filter(rule => rule !== '<implicit>').map(rule => {
    // if (rule.field === undefined || rule.value === undefined) {
    //     return '';
    // }
    if (rule.rules) {
      return '(' + formatQueryRules(rule) + ')';
    }
    if (rule.field) {
      return formatFieldRule(rule);
    }
    return formatOperator(rule);
  });
  return ruleStrings.join(' ');
};