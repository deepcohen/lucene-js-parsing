

const expect = require('chai').expect;

const lucene = require('../');

describe('formatQueryRules', () => {
  it('must handle empty ast', () => {
    const expected = { rules: [] };
    testAst('', expected);
    testAst('', expected);
    testAst('', expected);
  });

  it('must handle empty rules', () => {
    testStr('', { rules: [] });
  });

  it('must handle simple terms', () => {
    testStr('bar', { rules: [{ field: '<implicit>', value: 'bar', operator: '=' }]});
  });

  it('must handle simple terms with explicit field name', () => {
    testStr('foo:bar', { rules: [{ field: 'foo', value: 'bar', operator: '=' }]});
  });

  it.skip('must handle simple terms with explicit field name in round parentheses', () => {
    testStr('foo:(bar)',
      {
        rules: [
          {
            rules: [
              { field: 'foo', value: 'bar', operator: '=' }

            ]
          }
        ]
      });
  });

  it('must handle quoted terms', () => {
    testStr('"fizz buz"', { rules: [{ field: '<implicit>', value: 'fizz buz', operator: '=' }]});
  });

  it('must handle empty quoted terms', () => {
    testStr('""', { rules: [{ field: '<implicit>', value: '', operator: '=' }]});
  });

  it('must handle empty quoted terms and explicit fields', () => {
    testStr('name:""', { rules: [{ field: 'name', value: '', operator: '=' }]});
  });

  it('must handle <=""', () => {
    testStr('name:<=""', { rules: [{ field: 'name', value: '', operator: '<=' }]});
  });

  it('must handle >""', () => {
    testStr('name:>""', { rules: [{ field: 'name', value: '', operator: '>' }]});
  });

  it('must support field groups', () => {
    testStr('(foo:bar OR foo:baz)',
      {
        rules: [
          {
            rules: [
              { field: 'foo', value: 'bar', operator: '=' },
              'OR',
              { field: 'foo', value: 'baz', operator: '=' }
            ]
          }
        ]

      });
  });

  it('must support fuzzy', () => {
    testStr('foo~0.6',
      {
        rules: [
          { field: '<implicit>', value: 'foo', operator: '~', similarity: 0.6 },
        ]
      });
  });

  it('must support fuzzy without explicit similarity', () => {
    testStr('foo~',
      {
        rules: [
          { field: '<implicit>', value: 'foo', operator: '~', similarity: 0.5 },
        ]
      });
  });

  it('must support terms with \'-\'', () => {
    testStr('created_at:>now-5d',
      { 'rules': [{ 'field': 'created_at', 'operator': '>', 'value': 'now-5d' }] },
    );
  });

  it('must support terms with \'+\'', () => {
    testStr('created_at:>now+5d',
      { 'rules': [{ 'field': 'created_at', 'operator': '>', 'value': 'now+5d' }] },
    );
  });

  it.skip('must support regex terms', () => {
    testStr('/^fizz b?u[A-z]/',
      { 'rules': [{ 'field': '<implicit>', 'operator': '=', 'value': '^fizz b?u[A-z]' }] }
    );
  });

  it.skip('must support keyed regex terms', () => {
    testStr('some.key:/[mh]otel/',
      { 'rules': [{ 'field': 'some.key', 'operator': '=', 'value': '[mh]otel' }] }
    );
  });

  it('must support prefix operators (-)', () => {
    testStr('-bar',
      { 'rules': [{ 'field': '<implicit>', 'operator': '!=', 'value': 'bar' }] },
    );
  });

  it('must support prefix operators (-) on quoted terms', () => {
    testStr('-"fizz buzz"',
      { 'rules': [{ 'field': '<implicit>', 'operator': '!=', 'value': 'fizz buzz' }] },
    );
  });

  it('must support dates as terms', () => {
    testStr('foo:2015-01-01',
      { 'rules': [{ 'field': 'foo', 'operator': '=', 'value': '2015-01-01' }] }
    );
  });

  it('must support dots in field names', () => {
    testStr('sub.foo:bar',
      { 'rules': [{ 'field': 'sub.foo', 'operator': '=', 'value': 'bar' }] }
    );
  });

  it('must support quoted string with explicit field names', () => {
    testStr('foo:"fizz buzz"',
      { 'rules': [{ 'field': 'foo', 'operator': '=', 'value': 'fizz buzz' }] }
    );
  });

  it('must support empty quoted string with explicit field names', () => {
    testStr('foo:""',
      { 'rules': [{ 'field': 'foo', 'operator': '=', 'value': '' }] }
    );
  });

  it('must support prefixes and explicit field names (-)', () => {
    testStr('-foo:bar',
      { 'rules': [{ 'field': 'foo', 'operator': '!=', 'value': 'bar' }] }
    );
  });

  it('must support quoted prefixes', () => {
    testStr('-foo:"fizz buzz"',
      { 'rules': [{ 'field': 'foo', 'operator': '!=', 'value': 'fizz buzz' }] },
    );
  });

  it('must support implicit conjunction operators', () => {
    testStr('fizz buzz',
      {
        'rules': [
          { 'field': '<implicit>', 'operator': '=', 'value': 'fizz' },
          '<implicit>',
          { 'field': '<implicit>', 'operator': '=', 'value': 'buzz' }
        ]
      }
    );
  });

  it('must support explicit conjunction operators (OR)', () => {
    testStr('fizz OR buzz',
      {
        'rules': [
          { 'field': '<implicit>', 'operator': '=', 'value': 'fizz' },
          'OR',
          { 'field': '<implicit>', 'operator': '=', 'value': 'buzz' }
        ]
      }
    );
  });

  it('must support explicit conjunction operators (AND)', () => {
    testStr('fizz AND buzz',
      {
        'rules': [
          { 'field': '<implicit>', 'operator': '=', 'value': 'fizz' },
          'AND',
          { 'field': '<implicit>', 'operator': '=', 'value': 'buzz' }
        ]
      }
    );
  });

  it('must support explicit conjunction operators (NOT)', () => {
    testStr('fizz -buzz',
      {
        'rules': [
          { 'field': '<implicit>', 'operator': '=', 'value': 'fizz' },
          '<implicit>',
          { 'field': '<implicit>', 'operator': '!=', 'value': 'buzz' }
        ]
      }
    );
  });

  it('must support explicit conjunction operators (&&)', () => {
    testStr('fizz AND buzz',
      {
        'rules': [
          { 'field': '<implicit>', 'operator': '=', 'value': 'fizz' },
          'AND',
          { 'field': '<implicit>', 'operator': '=', 'value': 'buzz' }
        ]
      }
    );
  });

  it('must support explicit conjunction operators (||)', () => {
    testStr('fizz OR buzz',
      {
        'rules': [
          { 'field': '<implicit>', 'operator': '=', 'value': 'fizz' },
          'OR',
          { 'field': '<implicit>', 'operator': '=', 'value': 'buzz' }
        ]
      }
    );
  });

  it('must support parentheses groups', () => {
    testStr('fizz (buzz baz)',
      {
        'rules': [
          { 'field': '<implicit>', 'operator': '=', 'value': 'fizz' },
          '<implicit>',
          {
            'rules': [
              { 'field': '<implicit>', 'operator': '=', 'value': 'buzz' },
              '<implicit>',
              { 'field': '<implicit>', 'operator': '=', 'value': 'baz' }
            ]
          }
        ]
      }
    );
  });

  it('must support parentheses groups with explicit conjunction operators', () => {
    testStr('fizz AND (buzz OR baz)',
      {
        'rules': [
          { 'field': '<implicit>', 'operator': '=', 'value': 'fizz' },
          'AND',
          {
            'rules': [
              { 'field': '<implicit>', 'operator': '=', 'value': 'buzz' },
              'OR',
              { 'field': '<implicit>', 'operator': '=', 'value': 'baz' }
            ]
          }
        ]
      }
    );
  });

  it('must support inclusive range expressions', () => {
    testStr('foo:[bar TO baz]',
      { 'rules': [{ 'field': 'foo', 'operator': 'between', 'value': 'bar,baz' }] }
    );
  });

  it('must support range expressions with date ISO format', () => {
    testStr('date:[2017-11-17T01:32:45.123Z TO 2017-11-18T04:28:11.999Z]',
      {
        'rules': [
          { 'field': 'date', 'operator': 'between', 'value': '2017-11-17T01:32:45.123Z,2017-11-18T04:28:11.999Z' }
        ]
      }
    );
  });

  it('must support lucene example: title:"The Right Way" AND text:go', () => {
    testStr('title:"The Right Way" AND text:go',
      {
        'rules': [
          { 'field': 'title', 'operator': '=', 'value': 'The Right Way' },
          'AND',
          { 'field': 'text', 'operator': '=', 'value': 'go' }
        ]
      }
    );
  });

  it('must support lucene example: title:"Do it right" AND right', () => {
    testStr('title:"Do it right" AND right',
      {
        'rules': [
          { 'field': 'title', 'operator': '=', 'value': 'Do it right' },
          'AND',
          { 'field': '<implicit>', 'operator': '=', 'value': 'right' }
        ]
      }
    );
  });

  it('must support lucene example: title:Do it right', () => {
    testStr('title:Do it right',
      {
        'rules': [
          { 'field': 'title', 'operator': '=', 'value': 'Do' },
          '<implicit>',
          { 'field': '<implicit>', 'operator': '=', 'value': 'it' },
          '<implicit>',
          { 'field': '<implicit>', 'operator': '=', 'value': 'right' }
        ]
      }
    );
  });

  it('must support lucene example: te?t', () => {
    testStr('te?t',
      { 'rules': [{ 'field': '<implicit>', 'operator': '=', 'value': 'te?t' }] }
    );
  });

  it('must support lucene example: test*', () => {
    testStr('test*',
      { 'rules': [{ 'field': '<implicit>', 'operator': '=', 'value': 'test*' }] }
    );
  });

  it('must support lucene example: te*t', () => {
    testStr('te*t',
      { 'rules': [{ 'field': '<implicit>', 'operator': '=', 'value': 'te*t' }] }
    );
  });

  it('must support lucene example: roam~', () => {
    testStr('roam~',
      { 'rules': [{ 'field': '<implicit>', 'operator': '~', 'value': 'roam', 'similarity': 0.5 }] }
    );
  });

  it('must support lucene example: roam~0.8', () => {
    testStr('roam~0.8',
      { 'rules': [{ 'field': '<implicit>', 'operator': '~', 'value': 'roam', 'similarity': 0.8 }] }
    );
  });

  it('must support lucene example: "jakarta apache"~10', () => {
    testStr('"jakarta apache"~10',
      { 'rules': [{ 'field': '<implicit>', 'operator': '~', 'value': 'jakarta apache', 'proximity': 10 }] }
    );
  });

  it('must support lucene example: mod_date:[20020101 TO 20030101]', () => {
    testStr('mod_date:[20020101 TO 20030101]',
      { 'rules': [{ 'field': 'mod_date', 'operator': 'between', 'value': '20020101,20030101' }] }
    );
  });

  it('must support lucene example: title:{Aida TO Carmen}', () => {
    testStr('title:[Aida TO Carmen]',
      { 'rules': [{ 'field': 'title', 'operator': 'between', 'value': 'Aida,Carmen' }] }
    );
  });

  it('must support lucene example: jakarta apache', () => {
    testStr('jakarta apache',
      {
        'rules': [
          { 'field': '<implicit>', 'operator': '=', 'value': 'jakarta' },
          '<implicit>',
          { 'field': '<implicit>', 'operator': '=', 'value': 'apache' }
        ]
      }
    );
  });

  it('must support lucene example: "jakarta apache" jakarta', () => {
    testStr('"jakarta apache" jakarta',
      {
        'rules': [
          { 'field': '<implicit>', 'operator': '=', 'value': 'jakarta apache' },
          '<implicit>',
          { 'field': '<implicit>', 'operator': '=', 'value': 'jakarta' }
        ]
      }
    );
  });

  it('must support lucene example: "jakarta apache" OR jakarta', () => {
    testStr('"jakarta apache" OR jakarta',
      {
        'rules': [
          { 'field': '<implicit>', 'operator': '=', 'value': 'jakarta apache' },
          'OR',
          { 'field': '<implicit>', 'operator': '=', 'value': 'jakarta' }
        ]
      }
    );
  });

  it('must support lucene example: "jakarta apache" AND "Apache Lucene"', () => {
    testStr('"jakarta apache" AND "Apache Lucene"',
      {
        'rules': [
          { 'field': '<implicit>', 'operator': '=', 'value': 'jakarta apache' },
          'AND',
          { 'field': '<implicit>', 'operator': '=', 'value': 'Apache Lucene' }
        ]
      }
    );
  });

  it('must support lucene example: +jakarta lucene', () => {
    testStr('jakarta lucene',
      {
        'rules': [
          { 'field': '<implicit>', 'operator': '=', 'value': 'jakarta' },
          '<implicit>',
          { 'field': '<implicit>', 'operator': '=', 'value': 'lucene' }
        ]
      }
    );
  });

  it('must support lucene example: "jakarta apache" NOT "Apache Lucene"', () => {
    testStr('"jakarta apache" -"Apache Lucene"',
      {
        'rules': [
          { 'field': '<implicit>', 'operator': '=', 'value': 'jakarta apache' },
          '<implicit>',
          { 'field': '<implicit>', 'operator': '!=', 'value': 'Apache Lucene' }
        ]
      }
    );
  });

  it('must support lucene example: NOT "jakarta apache"', () => {
    testStr('-"jakarta apache"',
      { 'rules': [{ 'field': '<implicit>', 'operator': '!=', 'value': 'jakarta apache' }] }
    );
  });

  it('must support lucene example: "jakarta apache" -"Apache Lucene"', () => {
    testStr('"jakarta apache" -"Apache Lucene"',
      {
        'rules': [
          { 'field': '<implicit>', 'operator': '=', 'value': 'jakarta apache' },
          '<implicit>',
          { 'field': '<implicit>', 'operator': '!=', 'value': 'Apache Lucene' }
        ]
      }
    );
  });

  it('must support lucene example: (jakarta OR apache) AND website', () => {
    testStr('(jakarta OR apache) AND website',
      {
        'rules': [
          {
            'rules': [
              { 'field': '<implicit>', 'operator': '=', 'value': 'jakarta' },
              'OR',
              { 'field': '<implicit>', 'operator': '=', 'value': 'apache' },
            ]
          },
          'AND',
          { 'field': '<implicit>', 'operator': '=', 'value': 'website' }
        ]
      }
    );
  });

  it('must support lucene example: title:(+return +"pink panther")', () => {
    testStr('(title:return title:"pink panther")',
      {
        'rules': [
          {
            'rules': [
              { 'field': 'title', 'operator': '=', 'value': 'return' },
              '<implicit>',
              { 'field': 'title', 'operator': '=', 'value': 'pink panther' }
            ]
          }
        ]
      }
    );
  });

  it('must support strings with quotes', () => {
    testStr('foo:"start \\" end"',
      { 'rules': [{ 'field': 'foo', 'operator': '=', 'value': 'start \\" end' }] }
    );
  });

  it('must support escaped characters', () => {
    testStr('foo\\(\\)\\{\\}\\+\\~\\!\\?\\[\\]\\:\\*bar',
      { 'rules': [{ 'field': '<implicit>', 'operator': '=', 'value': 'foo\\(\\)\\{\\}\\+\\~\\!\\?\\[\\]\\:\\*bar' }] }
    );
  });

  it('must support escaped whitespace', () => {
    testStr('foo:"a\\ b"',
      { 'rules': [{ 'field': 'foo', 'operator': '=', 'value': 'a\\ b' }] }
    );
  });

  it('must support ! prefix operator', () => {
    testStr('-author:adams',
      { 'rules': [{ 'field': 'author', 'operator': '!=', 'value': 'adams' }] }
    );
  });

  it('must support parenthesized expressions with start set', () => {
    testStr('prop:value1 AND (-_exists_:other OR other:value2)',
      {
        'rules': [
          { 'field': 'prop', 'operator': '=', 'value': 'value1' },
          'AND',
          {
            'rules': [
              { 'field': '_exists_', 'operator': '!=', 'value': 'other' },
              'OR',
              { 'field': 'other', 'operator': '=', 'value': 'value2' }
            ]
          }
        ]
      }
    );
  });

  it('must support parenthesized expressions with start set and not', () => {
    testStr('prop:value1 AND (-_exists_:other OR other:value2)',
      {
        'rules': [
          { 'field': 'prop', 'operator': '=', 'value': 'value1' },
          'AND',
          {
            'rules': [
              { 'field': '_exists_', 'operator': '!=', 'value': 'other' },
              'OR',
              { 'field': 'other', 'operator': '=', 'value': 'value2' }
            ]
          }
        ]
      }
    );
  });

  function testAst(expected, rules) {
    const queryRules = lucene.formatQueryRules(rules);

    expect(queryRules).to.deep.equal(expected, 'Got the following rules: ' + JSON.stringify(queryRules));
  }

  function testStr(expected, rules) {
    testAst(expected, rules);
  }

});
